from face_service import Liveness,faceReg
from flask import Flask
from flask import request
from flask import jsonify

app = Flask(__name__)

def isian():
    yield ("Deta",2)
    yield ("Dira",3)
    yield ("Adiba",4)

@app.route("/liveness",methods=["GET"])
def liveness():
    video_path = request.args.get("video_url")
    data = []
    for a,b in liveness.liveness_test(video_path):
        data.append({"status":a,"proba":b})
        print(a,b)
    return jsonify(video_url=video_path,data=data)

@app.route("/recognition",methods=["GET"])
def recognition():
    #global faceReg
    img1 = request.args.get("img1_url")
    img2 = request.args.get("img2_url")
    eps = request.args.get("epsilon")
    matched = None
    cosineDistance = None
    #print("---------****---------------")
    #print(img1)
    #print("---------****---------------")
    #print(img2)
    if eps != None:
        matched,cosineDistance = facereg.verifyFace(img1,img2,float(eps))
        #return jsonify("Without Epsilon")
    else:
        matched,cosineDistance = facereg.verifyFace(img1,img2)
        #return jsonify("With EPsilon")
    return jsonify(status=matched,cosineDistance=cosineDistance,img1=img1,img2=img2,eps=eps)

@app.route("/")
def hello():
    return jsonify("Hello Bang")

if __name__ == "__main__":
    
    liveness=Liveness()
    facereg=faceReg()
    print("Face Service is On")
    app.run(port=5000)


