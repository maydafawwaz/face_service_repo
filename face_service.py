#from imutils.video import VideoStream
import tensorflow as tf
from keras.layers import Convolution2D,MaxPooling2D,ZeroPadding2D,Flatten,Activation,Dropout
from keras.models import Sequential,load_model,Model
from keras.preprocessing.image import load_img,img_to_array
from keras.applications.vgg16 import preprocess_input
import numpy as np
#import urllib
import requests
from PIL import Image
from io import BytesIO
import imutils
import pickle
import time
import cv2




class Liveness():
    def  __init__(self):
        print("[INFO] loading face detector...")
        self.protoPath = "face_detector/deploy.prototxt"
        self.modelPath = "face_detector/res10_300x300_ssd_iter_140000.caffemodel"
        self.net = cv2.dnn.readNetFromCaffe(self.protoPath, self.modelPath)

        self.model = load_model("liveness.model")
        self.le = pickle.loads(open("le.pickle", "rb").read())
        self.graph = tf.get_default_graph()

    def liveness_test(self,video_path):
        print("[INFO] starting video stream...")
        #vs = VideoStream(src=video_path).start()
        cap = cv2.VideoCapture(video_path)
        time.sleep(2.0)

        # loop over the frames from the video stream
        while (cap.isOpened()):
            # grab the frame from the threaded video stream and resize it
            # to have a maximum width of 600 pixels
            #frame = vs.read()
            ret, frame = cap.read()
            frame = imutils.resize(frame, width=600)

            # grab the frame dimensions and convert it to a blob
            (h, w) = frame.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                (300, 300), (104.0, 177.0, 123.0))

            # pass the blob through the network and obtain the detections and
            # predictions
            self.net.setInput(blob)
            detections = self.net.forward()

            # loop over the detections
            for i in range(0, detections.shape[2]):
                # extract the confidence (i.e., probability) associated with the
                # prediction
                confidence = detections[0, 0, i, 2]

                # filter out weak detections
                if confidence > 0.5:
                    # compute the (x, y)-coordinates of the bounding box for
                    # the face and extract the face ROI
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")

                    # ensure the detected bounding box does fall outside the
                    # dimensions of the frame
                    startX = max(0, startX)
                    startY = max(0, startY)
                    endX = min(w, endX)
                    endY = min(h, endY)

                    # extract the face ROI and then preproces it in the exact
                    # same manner as our training data
                    face = frame[startY:endY, startX:endX]
                    face = cv2.resize(face, (32, 32))
                    face = face.astype("float") / 255.0
                    face = img_to_array(face)
                    face = np.expand_dims(face, axis=0)

                    # pass the face ROI through the trained liveness detector
                    # model to determine if the face is "real" or "fake"
                    with self.graph.as_default():
                        preds = self.model.predict(face)[0]
                    j = np.argmax(preds)
                    label = self.le.classes_[j]
                    yield label,preds[j]
                    # draw the label and bounding box on the frame
                    #label = "{}: {:.4f}".format(label, preds[j])
                    #cv2.putText(frame, label, (startX, startY - 10),
                       #cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                    #cv2.rectangle(frame, (startX, startY), (endX, endY),
                        #(0, 0, 255), 2)

            # show the output frame and wait for a key press
            #cv2.imshow("Frame", frame)
            #key = cv2.waitKey(1) & 0xFF

            # if the `q` key was pressed, break from the loop
            #if key == ord("q"):
                #break

        # do a bit of cleanup
        #cv2.destroyAllWindows()
        #vs.stop()
        cap.release()
    
class faceReg():

    def __init__(self):
        print("Loading Model")
        self.vgg_face_reg = self.load_model()
        self.graph = tf.get_default_graph()
    def url_to_image(self,image_path):
        resp=urllib.requests
    def preprocess_image(self,image_path):
        data_bytes = requests.get(image_path).content
        img = Image.open(BytesIO(data_bytes))
        img = img.resize((224,224))
        #img = load_img(image_path, target_size=(224, 224))
        img = img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        return img

    def findCosineSimiliarity(self,source_reps,test_reps):
        #-1 for exact opposite and 1 for exact the same
        a = np.matmul(np.transpose(source_reps),test_reps)
        b = np.sum(np.multiply(source_reps,source_reps))
        c = np.sum(np.multiply(test_reps,test_reps))
        return a/(np.sqrt(b)*np.sqrt(c))

    def findCosineDistance(self,source_representation, test_representation):
        # 1- cosineSimiliarity
        # closer to zero more similiar
        return 1 - self.findCosineSimiliarity(source_representation,test_representation)

    def verifyFace(self,img1, img2,epsilon=0.40):
        #img1,img2 is path
        with self.graph.as_default():
            img1_representation = self.vgg_face_reg.predict(self.preprocess_image(img1))[0,:]
            img2_representation = self.vgg_face_reg.predict(self.preprocess_image(img2))[0,:]
    
        cosine_distance = self.findCosineDistance(img1_representation, img2_representation)
        #euclidean_distance = findEuclideanDistance(img1_representation, img2_representation)
        isSame = False
        if(cosine_distance < epsilon):
            isSame = True
        return isSame,cosine_distance

    def load_model(self):
        
        model = Sequential()
        model.add(ZeroPadding2D((1,1),input_shape=(224,224, 3)))
        model.add(Convolution2D(64, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D((2,2), strides=(2,2)))
    
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(128, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(128, (3, 3), activation='relu'))
        model.add(MaxPooling2D((2,2), strides=(2,2)))
    
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(256, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(256, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(256, (3, 3), activation='relu'))
        model.add(MaxPooling2D((2,2), strides=(2,2)))
    
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(512, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(512, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(512, (3, 3), activation='relu'))
        model.add(MaxPooling2D((2,2), strides=(2,2)))
    
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(512, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(512, (3, 3), activation='relu'))
        model.add(ZeroPadding2D((1,1)))
        model.add(Convolution2D(512, (3, 3), activation='relu'))
        model.add(MaxPooling2D((2,2), strides=(2,2)))
    
        model.add(Convolution2D(4096, (7, 7), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Convolution2D(4096, (1, 1), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Convolution2D(2622, (1, 1)))
        model.add(Flatten())
        model.add(Activation('softmax'))
        model.load_weights('vgg_face_weights.h5')
        return Model(inputs=model.layers[0].input,outputs=model.layers[-2].output)

#if __name__=="__main__":
    #ffr=faceReg()
    #print(ffr.verifyFace("http://i64.tinypic.com/34xric9.jpg","http://i63.tinypic.com/ru722o.jpg"))
    #ggr = Liveness()
    #for (a,b) in ggr.liveness_test("irene.mp4"):
        #print(a,b)
    #?img1_url=http://i64.tinypic.com/34xric9.jpg&img2_url=http://i63.tinypic.com/ru722o.jpg&epsilon=0.4